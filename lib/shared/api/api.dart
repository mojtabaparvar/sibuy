import 'package:dio/dio.dart';

class SingletonDio {
  static final SingletonDio _singlton = new SingletonDio._internal();
  SingletonDio._internal();
  static SingletonDio get instance => _singlton;
  Dio dio;
}
