import 'package:json_annotation/json_annotation.dart';
part 'server-response.model.g.dart';

@JsonSerializable(nullable: true)
class ServerResponseModel {
  final PaginationModel pagination;
  var data;

  ServerResponseModel({this.pagination, this.data});
  factory ServerResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ServerResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$ServerResponseModelToJson(this);
}

@JsonSerializable(nullable: true)
class PaginationModel {
  final int current_page;
  final int total;
  final int last_page;
  final int per_page;

  PaginationModel(this.current_page, this.total, this.last_page, this.per_page);
  factory PaginationModel.fromJson(Map<String, dynamic> json) =>
      _$PaginationModelFromJson(json);
  Map<String, dynamic> toJson() => _$PaginationModelToJson(this);
}
