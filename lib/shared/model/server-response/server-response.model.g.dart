// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server-response.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerResponseModel _$ServerResponseModelFromJson(Map<String, dynamic> json) {
  return ServerResponseModel(
    pagination: json['pagination'] == null
        ? null
        : PaginationModel.fromJson(json['pagination'] as Map<String, dynamic>),
    data: json['data'],
  );
}

Map<String, dynamic> _$ServerResponseModelToJson(
        ServerResponseModel instance) =>
    <String, dynamic>{
      'pagination': instance.pagination,
      'data': instance.data,
    };

PaginationModel _$PaginationModelFromJson(Map<String, dynamic> json) {
  return PaginationModel(
    json['current_page'] as int,
    json['total'] as int,
    json['last_page'] as int,
    json['per_page'] as int,
  );
}

Map<String, dynamic> _$PaginationModelToJson(PaginationModel instance) =>
    <String, dynamic>{
      'current_page': instance.current_page,
      'total': instance.total,
      'last_page': instance.last_page,
      'per_page': instance.per_page,
    };
