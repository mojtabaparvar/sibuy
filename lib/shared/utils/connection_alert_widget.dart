import 'package:flutter/material.dart';

import 'connection_alert.dart';
import 'connection_service.dart';

class ConnectionAlertWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final connectionStatus = ConnectionStatusSingleton.getInstance();
    return StreamBuilder(
      stream: connectionStatus.connectionChange,
      initialData: true,
      builder: (context, snapshot) {
        if (snapshot.data == false) {
          return ConnectionAlert.getInstance();
        }
        return SizedBox();
      },
    );
  }
}
