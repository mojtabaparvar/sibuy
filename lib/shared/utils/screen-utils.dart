import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:sibuy/shared/utils/get_hex.dart';

Color primaryColor = Color(getColorHexFromStr("#9253fb"));
Color accentColor = Color(getColorHexFromStr("#eef1f6"));
Color txtColor = Color(getColorHexFromStr("#9ea6b4"));

sibuyInputDecoration(label, icon) {
  return InputDecoration(
    contentPadding: EdgeInsets.only(right: 5),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    labelText: label,
    labelStyle: TextStyle(color: txtColor, fontWeight: FontWeight.bold),
    suffixIcon: Icon(
      icon,
      color: txtColor,
    ),
  );
}

class SibuyInput extends StatelessWidget {
  SibuyInput(this.label, this.controller, this.icon);
  final label;
  final controller;
  final icon;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15),
      width: MediaQuery.of(context).size.width,
      height: 50,
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(15)),
      child: TextFormField(
        textAlignVertical: TextAlignVertical.center,
        style: new TextStyle(decoration: TextDecoration.none),
        controller: controller,
        validator: (val) {
          if (val.isEmpty) {
            return "please enter $label ";
          }
          return null;
        },
        decoration: sibuyInputDecoration(label, icon),
        onChanged: (val) {},
      ),
    );
  }
}

Image showChachImage(String url) {
  return Image(
    image: AdvancedNetworkImage(
      url,
      useDiskCache: true,
      cacheRule: CacheRule(maxAge: const Duration(days: 7)),
    ),
    fit: BoxFit.cover,
  );
}

showSuccessSnack(context, txt) {
  Scaffold.of(context).showSnackBar(SnackBar(
    content: Text(
      txt,
      textAlign: TextAlign.left,
    ),
    backgroundColor: Colors.green,
  ));
}

showErrorSnack(context, txt) {
  Scaffold.of(context).showSnackBar(SnackBar(
    content: Text(
      txt,
      textAlign: TextAlign.left,
    ),
    backgroundColor: Colors.red,
  ));
}
