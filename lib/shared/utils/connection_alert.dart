import 'package:flutter/material.dart';

import 'connection_service.dart';

class ConnectionAlert extends StatelessWidget {
  static final ConnectionAlert _singleton = ConnectionAlert._internal();
  ConnectionAlert._internal();
  factory ConnectionAlert() {
    return _singleton;
  }
  static ConnectionAlert getInstance() => _singleton;
  @override
  Widget build(BuildContext context) {
    final connectionStatus = ConnectionStatusSingleton.getInstance();
    return StreamBuilder(
      stream: connectionStatus.connectionChange,
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data == true) {
          return SizedBox();
        }
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.grey.withOpacity(0.4),
          child: Center(
            child: RawMaterialButton(
              elevation: 6,
              onPressed: null,
              child: Container(
                width: 250,
                height: 60,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(color: Colors.black, boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    blurRadius: 5.0, // has the effect of softening the shadow
                    spreadRadius: 4.0, // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ]),
                padding: EdgeInsets.all(10),
                child: Center(
                  child: Text(
                    'Please check yor internet connection',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      height: 2,
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
