import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'cancelable-future.dart';

Future<bool> confirmExit(BuildContext context) async {
  CancelableFuture cancelableFuture = new CancelableFuture();
  await showDialog(
      context: context,
      builder: (context) {
        cancelableFuture.cancelableFuture(Duration(milliseconds: 2500), () {
          Navigator.of(context).pop(false);
        });
        return AlertDialog(
              content: new Text(
                "exit application?",
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: new Text(
                    'No',
                  ),
                ),
                new FlatButton(
                  onPressed: () => SystemChannels.platform
                      .invokeMethod('SystemNavigator.pop'),
                  child: new Text(
                    "Yes",
                    style: TextStyle(
                      fontFamily: "Ir",
                      color: Colors.red,
                    ),
                    textDirection: TextDirection.rtl,
                    textAlign: TextAlign.end,
                  ),
                ),
              ],
            ) ??
            false;
      }).then((value) => cancelableFuture.cancel());
}
