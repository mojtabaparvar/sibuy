import 'package:flutter/foundation.dart';
import 'dart:async';

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;
  bool isCanceled = false;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (!isCanceled) {
      if (_timer != null) {
        _timer.cancel();
      }

      _timer = Timer(Duration(milliseconds: milliseconds), action);
    }
  }

  close() {
    isCanceled = true;
  }
}
