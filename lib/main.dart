import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:sibuy/main-page.dart';
import 'package:sibuy/search-page/bloc/search_page_bloc.dart';
import 'package:sibuy/shared/api/api.dart';
import 'package:sibuy/shared/utils/screen-utils.dart';
import 'package:sibuy/shared/utils/urls.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  crateDioInstance();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<SearchPageBloc>(
          create: (BuildContext context) => SearchPageBloc(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

crateDioInstance() {
  var dioInstance = SingletonDio.instance;
  BaseOptions options = new BaseOptions(
    baseUrl: URLs.apiUrl,
    connectTimeout: 20000,
    receiveTimeout: 3000,
  );
  dioInstance.dio = new Dio(options);
  dioInstance.dio.interceptors.add(
    PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90),
  );
  dioInstance.dio.interceptors
      .add(DioCacheManager(CacheConfig(baseUrl: URLs.apiUrl)).interceptor);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'sibuy',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryColor,
        accentColor: accentColor,
        textTheme:
            GoogleFonts.robotoTextTheme(Theme.of(context).textTheme).copyWith(
          headline1: GoogleFonts.roboto(
              textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16)),
          bodyText1: GoogleFonts.roboto(
              textStyle: TextStyle(color: Colors.white, fontSize: 15)),
        ),
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.android: CupertinoPageTransitionsBuilder()
          },
        ),
        scaffoldBackgroundColor: Colors.white,
      ),
      home: MainPage(),
    );
  }
}

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}
