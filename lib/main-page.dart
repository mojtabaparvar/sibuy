import 'package:flutter/material.dart';
import 'package:sibuy/search-page/page/search-page.dart';
import 'package:sibuy/shared/utils/confirm-exit.dart';
import 'package:sibuy/shared/utils/connection_alert_widget.dart';
import 'package:sibuy/shared/utils/connection_service.dart';
import 'package:sibuy/shared/utils/screen-utils.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = <Widget>[
    Text(
      'Shop',
    ),
    SearchPage(),
    Text(
      'Add Page',
    ),
    Text(
      'Notif Page',
    ),
    Text(
      'Profile Page',
    ),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    final connectionStatus = ConnectionStatusSingleton.getInstance();
    connectionStatus.initialize();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => confirmExit(context),
      child: Scaffold(
        body: Center(
          child: Stack(
            children: [
              _widgetOptions.elementAt(_selectedIndex),
              ConnectionAlertWidget()
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.grey,
          backgroundColor: Colors.white,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.store),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_box),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications_active),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outline),
              title: Text(''),
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          selectedItemColor: primaryColor,
        ),
      ),
    );
  }
}
