import 'package:sibuy/shared/api/api.dart';
import 'package:sibuy/shared/model/server-response/server-response.model.dart';

class SearchPageApi {
  var dio = SingletonDio.instance.dio;
  Future<ServerResponseModel> getCategoryData() async {
    try {
      var res = await dio.get(dio.options.baseUrl + "categories");
      return ServerResponseModel.fromJson(res.data);
    } catch (e) {
      throw Exception("failed at getting categoris");
    }
  }

  Future<ServerResponseModel> getCategoryItems(int id) async {
    try {
      var res = await dio.get(dio.options.baseUrl + "categories/$id");
      return ServerResponseModel.fromJson(res.data);
    } catch (e) {
      throw Exception("failed at getting categoris");
    }
  }
}
