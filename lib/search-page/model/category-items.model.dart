import 'package:json_annotation/json_annotation.dart';
part 'category-items.model.g.dart';

@JsonSerializable(nullable: true)
class CategoryItemsModel {
  final int id;
  final String title;
  final String source;
  final String description;
  final String format;
  final bool is_main;
  final bool is_followed;
  final bool is_bought;
  final int view_count;
  final String rate;
  final List<String> tags;
  final CategoryItemProductModel product;
  final CategoryItemCategoryModel category;

  CategoryItemsModel(
      this.id,
      this.title,
      this.source,
      this.description,
      this.format,
      this.is_main,
      this.is_followed,
      this.is_bought,
      this.view_count,
      this.rate,
      this.tags,
      this.product,
      this.category);
  factory CategoryItemsModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemsModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryItemsModelToJson(this);
}

@JsonSerializable(nullable: true)
class CategoryItemProductModel {
  final int id;
  final String title;
  final String price;
  final int user_id;
  final String created_at;
  final String updated_at;

  CategoryItemProductModel(this.id, this.title, this.price, this.user_id,
      this.created_at, this.updated_at);
  factory CategoryItemProductModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemProductModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryItemProductModelToJson(this);
}

@JsonSerializable(nullable: true)
class CategoryItemCategoryModel {
  final String title;
  final int view_count;

  CategoryItemCategoryModel(this.title, this.view_count);
  factory CategoryItemCategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemCategoryModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryItemCategoryModelToJson(this);
}
