// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category-items.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryItemsModel _$CategoryItemsModelFromJson(Map<String, dynamic> json) {
  return CategoryItemsModel(
    json['id'] as int,
    json['title'] as String,
    json['source'] as String,
    json['description'] as String,
    json['format'] as String,
    json['is_main'] as bool,
    json['is_followed'] as bool,
    json['is_bought'] as bool,
    json['view_count'] as int,
    json['rate'] as String,
    (json['tags'] as List)?.map((e) => e as String)?.toList(),
    json['product'] == null
        ? null
        : CategoryItemProductModel.fromJson(
            json['product'] as Map<String, dynamic>),
    json['category'] == null
        ? null
        : CategoryItemCategoryModel.fromJson(
            json['category'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CategoryItemsModelToJson(CategoryItemsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'source': instance.source,
      'description': instance.description,
      'format': instance.format,
      'is_main': instance.is_main,
      'is_followed': instance.is_followed,
      'is_bought': instance.is_bought,
      'view_count': instance.view_count,
      'rate': instance.rate,
      'tags': instance.tags,
      'product': instance.product,
      'category': instance.category,
    };

CategoryItemProductModel _$CategoryItemProductModelFromJson(
    Map<String, dynamic> json) {
  return CategoryItemProductModel(
    json['id'] as int,
    json['title'] as String,
    json['price'] as String,
    json['user_id'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
  );
}

Map<String, dynamic> _$CategoryItemProductModelToJson(
        CategoryItemProductModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'price': instance.price,
      'user_id': instance.user_id,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
    };

CategoryItemCategoryModel _$CategoryItemCategoryModelFromJson(
    Map<String, dynamic> json) {
  return CategoryItemCategoryModel(
    json['title'] as String,
    json['view_count'] as int,
  );
}

Map<String, dynamic> _$CategoryItemCategoryModelToJson(
        CategoryItemCategoryModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'view_count': instance.view_count,
    };
