// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search-page.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoriesModel _$CategoriesModelFromJson(Map<String, dynamic> json) {
  return CategoriesModel(
    json['id'] as int,
    json['title'] as String,
    json['view_count'] as int,
  );
}

Map<String, dynamic> _$CategoriesModelToJson(CategoriesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'view_count': instance.view_count,
    };
