import 'package:json_annotation/json_annotation.dart';
part 'search-page.model.g.dart';

@JsonSerializable(nullable: true)
class CategoriesModel {
  final int id;
  final String title;
  final int view_count;

  CategoriesModel(this.id, this.title, this.view_count);
  factory CategoriesModel.fromJson(Map<String, dynamic> json) =>
      _$CategoriesModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoriesModelToJson(this);
}
