import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sibuy/search-page/bloc/cat-item-bloc/category_items_bloc.dart';
import 'package:sibuy/search-page/bloc/search_page_bloc.dart';
import 'package:sibuy/search-page/model/category-items.model.dart';
import 'package:sibuy/shared/utils/debouncer.dart';
import 'package:sibuy/shared/utils/screen-utils.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  CategoryItemsBloc _categoryItemsBloc = new CategoryItemsBloc();
  int _activeCatId;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final _debouncer = Debouncer(milliseconds: 1000);
  TextEditingController _searchController = new TextEditingController();
  @override
  void initState() {
    BlocProvider.of<SearchPageBloc>(context).add(SearchPageStart());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchPageBloc, SearchPageState>(
      listener: (context, state) {
        if (state is SearchPageError) {
          showErrorSnack(context, state.error);
        }
        if (state is SearchPageLoaded) {
          _activeCatId = state.cats[0].id;
          _categoryItemsBloc.add(CategoryItemsStart(_activeCatId));
        }
      },
      child: BlocBuilder<SearchPageBloc, SearchPageState>(
        builder: (context, state) {
          if (state is SearchPageLoaded) {
            return SmartRefresher(
              controller: _refreshController,
              enablePullDown: true,
              enablePullUp: false,
              onRefresh: () {
                _debouncer.run(() => BlocProvider.of<SearchPageBloc>(context)
                    .add(SearchPageReStart()));
                _refreshController.refreshCompleted();
              },
              header: WaterDropMaterialHeader(
                distance: 50,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 18.0),
                        child: SibuyInput(
                            "Tap To Search", _searchController, Icons.search),
                      ),
                    ),
                    Container(
                      height: 70,
                      child: ListView.builder(
                        padding: EdgeInsets.only(left: 20),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.cats.length,
                        itemBuilder: (ctx, ind) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  _activeCatId = state.cats[ind].id;
                                });
                                _categoryItemsBloc
                                    .add(CategoryItemsStart(_activeCatId));
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    state.cats[ind].title,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold,
                                        color:
                                            _activeCatId == state.cats[ind].id
                                                ? Colors.black
                                                : txtColor),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  AnimatedOpacity(
                                      opacity:
                                          _activeCatId == state.cats[ind].id
                                              ? 1
                                              : 0,
                                      duration: Duration(milliseconds: 200),
                                      child: Container(
                                          height: 2.5,
                                          width: 20,
                                          color:
                                              Theme.of(context).primaryColor)),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height - 120,
                      child: BlocBuilder<CategoryItemsBloc, CategoryItemsState>(
                        bloc: _categoryItemsBloc,
                        builder: (context, iState) {
                          if (iState is CategoryItemsLoaded) {
                            return GridView.builder(
                                padding: EdgeInsets.symmetric(horizontal: 1),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisSpacing: 1,
                                        crossAxisCount: 2,
                                        childAspectRatio: 0.7),
                                itemCount: iState.items.length,
                                itemBuilder: (ctx, ind) {
                                  return ItemWidget(item: iState.items[ind]);
                                });
                          }
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ItemWidget extends StatefulWidget {
  final CategoryItemsModel item;

  const ItemWidget({Key key, @required this.item}) : super(key: key);
  @override
  _ItemWidgetState createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  String _tempDir;

  @override
  void initState() {
    super.initState();
    getTemporaryDirectory().then((d) => _tempDir = d.path);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          width: double.infinity,
          child: GenThumbnailImage(
            thumbnailRequest: ThumbnailRequest(
                video: widget.item.source,
                thumbnailPath: _tempDir,
                imageFormat: ImageFormat.JPEG,
                maxHeight: 1000,
                maxWidth: 1000,
                quality: 90),
          ),
        ),
        Container(
          height: 60,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                Colors.black12.withOpacity(0.05),
                Colors.black12.withOpacity(0.06),
                Colors.black12.withOpacity(0.07),
              ])),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.item.description ?? "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(widget.item.product.price + "\$" ?? "",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17)),
                    Row(
                      children: [
                        Text(widget.item.rate ?? "",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16)),
                        Icon(
                          Icons.star,
                          color: Colors.white,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

class ThumbnailRequest {
  final String video;
  final String thumbnailPath;
  final ImageFormat imageFormat;
  final int maxHeight;
  final int maxWidth;
  final int timeMs;
  final int quality;

  const ThumbnailRequest(
      {this.video,
      this.thumbnailPath,
      this.imageFormat,
      this.maxHeight,
      this.maxWidth,
      this.timeMs,
      this.quality});
}

class ThumbnailResult {
  final Image image;
  final int dataSize;
  final int height;
  final int width;
  const ThumbnailResult({this.image, this.dataSize, this.height, this.width});
}

Future<ThumbnailResult> genThumbnail(ThumbnailRequest r) async {
  Uint8List bytes;
  final Completer<ThumbnailResult> completer = Completer();
  if (r.thumbnailPath != null) {
    final thumbnailPath = await VideoThumbnail.thumbnailFile(
        video: r.video,
        thumbnailPath: r.thumbnailPath,
        imageFormat: r.imageFormat,
        maxHeight: r.maxHeight,
        maxWidth: r.maxWidth,
        timeMs: r.timeMs,
        quality: r.quality);

    print("thumbnail file is located: $thumbnailPath");

    final file = File(thumbnailPath);
    bytes = file.readAsBytesSync();
  } else {
    bytes = await VideoThumbnail.thumbnailData(
        video: r.video,
        imageFormat: r.imageFormat,
        maxHeight: r.maxHeight,
        maxWidth: r.maxWidth,
        timeMs: r.timeMs,
        quality: r.quality);
  }

  int _imageDataSize = bytes.length;
  print("image size: $_imageDataSize");

  final _image = Image.memory(bytes, fit: BoxFit.fill);
  _image.image
      .resolve(ImageConfiguration())
      .addListener(ImageStreamListener((ImageInfo info, bool _) {
    completer.complete(ThumbnailResult(
      image: _image,
      dataSize: _imageDataSize,
      height: info.image.height,
      width: info.image.width,
    ));
  }));
  return completer.future;
}

class GenThumbnailImage extends StatefulWidget {
  final ThumbnailRequest thumbnailRequest;

  const GenThumbnailImage({Key key, this.thumbnailRequest}) : super(key: key);

  @override
  _GenThumbnailImageState createState() => _GenThumbnailImageState();
}

class _GenThumbnailImageState extends State<GenThumbnailImage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ThumbnailResult>(
      future: genThumbnail(widget.thumbnailRequest),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          final _image = snapshot.data.image;

          return _image;
        } else if (snapshot.hasError) {
          return Container();
        } else {
          return Container();
        }
      },
    );
  }
}
