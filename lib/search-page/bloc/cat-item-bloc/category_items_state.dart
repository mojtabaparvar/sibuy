part of 'category_items_bloc.dart';

abstract class CategoryItemsState extends Equatable {
  const CategoryItemsState();
}

class CategoryItemsInitial extends CategoryItemsState {
  @override
  List<Object> get props => [];
}

class CategoryItemsLoading extends CategoryItemsState {
  @override
  List<Object> get props => [];
}

class CategoryItemsLoaded extends CategoryItemsState {
  final List<CategoryItemsModel> items;

  CategoryItemsLoaded(this.items);
  @override
  List<Object> get props => [];
}

class CategoryItemsError extends CategoryItemsState {
  final String error;

  CategoryItemsError(this.error);
  @override
  List<Object> get props => [];
}
