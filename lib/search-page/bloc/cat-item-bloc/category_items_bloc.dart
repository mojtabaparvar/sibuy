import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sibuy/search-page/model/category-items.model.dart';
import 'package:sibuy/search-page/resource/api.dart';

part 'category_items_event.dart';
part 'category_items_state.dart';

class CategoryItemsBloc extends Bloc<CategoryItemsEvent, CategoryItemsState> {
  @override
  CategoryItemsState get initialState => CategoryItemsInitial();
  SearchPageApi _api = new SearchPageApi();

  @override
  Stream<CategoryItemsState> mapEventToState(
    CategoryItemsEvent event,
  ) async* {
    if (event is CategoryItemsStart) {
      yield CategoryItemsLoading();
      try {
        var data = await _api.getCategoryItems(event.id);
        List<CategoryItemsModel> items = [];
        for (var it in data.data) {
          items.add(CategoryItemsModel.fromJson(it));
        }
        yield CategoryItemsLoaded(items);
      } catch (e) {
        yield CategoryItemsError("some thing went wrong");
      }
    }
  }
}
