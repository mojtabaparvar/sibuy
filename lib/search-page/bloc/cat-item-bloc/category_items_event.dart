part of 'category_items_bloc.dart';

abstract class CategoryItemsEvent extends Equatable {
  const CategoryItemsEvent();
}

class CategoryItemsStart extends CategoryItemsEvent {
  final int id;

  CategoryItemsStart(this.id);
  @override
  List<Object> get props => [];
}
