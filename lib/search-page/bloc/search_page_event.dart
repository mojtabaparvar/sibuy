part of 'search_page_bloc.dart';

abstract class SearchPageEvent extends Equatable {
  const SearchPageEvent();
}

class SearchPageStart extends SearchPageEvent {
  @override
  List<Object> get props => [];
}

class SearchPageReStart extends SearchPageEvent {
  @override
  List<Object> get props => [];
}
