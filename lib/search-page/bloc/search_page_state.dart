part of 'search_page_bloc.dart';

abstract class SearchPageState extends Equatable {
  const SearchPageState();
}

class SearchPageInitial extends SearchPageState {
  @override
  List<Object> get props => [];
}

class SearchPageLoading extends SearchPageState {
  @override
  List<Object> get props => [];
}

class SearchPageLoaded extends SearchPageState {
  final List<CategoriesModel> cats;

  SearchPageLoaded({@required this.cats});
  @override
  List<Object> get props => [];
}

class SearchPageError extends SearchPageState {
  final String error;

  SearchPageError(this.error);
  @override
  List<Object> get props => [];
}
