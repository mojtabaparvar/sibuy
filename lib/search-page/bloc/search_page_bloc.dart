import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:sibuy/search-page/model/search-page.model.dart';
import 'package:sibuy/search-page/resource/api.dart';

part 'search_page_event.dart';
part 'search_page_state.dart';

class SearchPageBloc extends Bloc<SearchPageEvent, SearchPageState> {
  @override
  SearchPageState get initialState => SearchPageInitial();
  SearchPageApi _api = new SearchPageApi();
  @override
  Stream<SearchPageState> mapEventToState(
    SearchPageEvent event,
  ) async* {
    if (event is SearchPageStart && state is! SearchPageLoaded) {
      yield SearchPageLoading();
      try {
        var cats = (await _api.getCategoryData()).data;
        List<CategoriesModel> categoriesModel = [];
        for (var cat in cats) {
          categoriesModel.add(CategoriesModel.fromJson(cat));
        }
        yield SearchPageLoaded(cats: categoriesModel);
      } catch (e) {
        yield SearchPageError("Some Thing Went wrong!");
      }
    }
    if (event is SearchPageReStart) {
      yield SearchPageLoading();
      try {
        var cats = (await _api.getCategoryData()).data;
        List<CategoriesModel> categoriesModel = [];
        for (var cat in cats) {
          categoriesModel.add(CategoriesModel.fromJson(cat));
        }
        yield SearchPageLoaded(cats: categoriesModel);
      } catch (e) {
        yield SearchPageError("Some Thing Went wrong!");
      }
    }
  }
}
